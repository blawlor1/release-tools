#!/usr/bin/env perl


use Modern::Perl;
use File::FindLib 'lib';
use Koha::Release;
use Pod::Usage;
use Getopt::Long;
use YAML qw(Dump);

binmode( STDOUT, ':encoding(utf8)' );

my ( $verbose, $help, $silent, $range, $branch ) = ( 0, 0, 0, '', undef );

GetOptions(
    'verbose'   => \$verbose,
    'help'      => \$help,
    'silent'    => \$silent,
    'range=s'   => \$range,
    'branch=s'  => \$branch,
);

sub usage { 
    pod2usage( -verbose => 2 );
    exit;
}

usage() if $help || @ARGV < 2;



my $release = Koha::Release->new( range => shift @ARGV, silent => $silent );

if ( my $cmd = shift @ARGV ) {
    if ( $cmd =~ /notes/i ) {
        $release->build_notes(@ARGV);
    }
    elsif ( $cmd =~ /html/i ) {
        my $html = shift @ARGV;
        usage() unless ($html);
        $release->build_html($html);
    }
    elsif ( $cmd =~ /info/i ) {
        print Dump($release->info());
    }
    elsif ( $cmd =~ /bug/i ) {
        my $response = $release->bz->get(
            'bug/' . join(',', @ARGV) 
            #'/comment'
            #'&include_fields=severity,creator_detail,component'
        );
        say Dump($response);
    }
    elsif ( $cmd =~ /updatebz/i ) {
        $release->updatebz();
    }
    elsif ( $cmd =~ /tarball/ ) {
        unless ($branch) {
            usage();
            die "--branch mandatory for tarball";
        }

        $release->tarball( $branch );
    }
}



=head1 NAME

koha-release - Koha release tools

=head1 SYNOPSYS

  koha-release v3.22.01..HEAD info
  koha-release v3.22.01..HEAD notes
  koha-release v3.22.01..HEAD html notes.html
  koha-release v3.22.01..HEAD updatebz
  koha-release v3.22.01..HEAD --branch xxx tarball

=head1 DESCRIPTION

The C<koha-release> script can be used to generates release notes and update
bugzilla.

It retrieves all bugs in Git for a commits range. For this reason, it must be
launched from the root directory of a Koha Git repository. The bug descriptions
are retrieved from bugzilla using BZ REST API. It also generates contributor,
signer & sponsor lists (using Git information), and a translation percentage
per language.

The script works on a range of Git commits. This range generally has to cover
commits from the last released version to the current one. For example, if the
current version to be published is 3.22.3, and the previous one was tagged
v3.22.02, this would be a valid range: v3.22.02..HEAD.

The script is parametrized with a configuration file: C<etc/config.yaml>. See
it for details. There is one Template Toolkit file used to generate the
Markdown release notes file: C<etc/notes.tt>.

Five commands:

=over

=item C<koha-release range info>

Outputs raw info in YAML format about the release.

=item C<koha-release range notes>

Outputs the release notes in Markdown.

=item C<koha-release range html notes.html>

Outputs the release notices in HTML by converting the Markdown version
previously generated.

=item C<koha-release range updatebz>

This will retrieve all bugs from bugzilla that are present in the given commit
range, check that they are marked as 'Pushed to X', where X is your release
branch, and then mark those bugs as 'RESOLVED FIXED'.

=item C<koha-release range --branch xxx tarball>

This will generate the tarball for the release. Requires I<user.yaml> to have a
B<release.dir> entry. See I<user.example>.

=back

=cut
